//
//  Extensions.swift
//  SocialApp
//
//  Created by ABBC on 4/22/21.
//

import Foundation
import UIKit
extension UIViewController {
    
    //MARK : alert message to show in all view controller
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in}
        alertController.addAction(OKAction)
         alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
