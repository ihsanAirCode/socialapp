//
//  HomeViewController.swift
//  SocialApp
//
//  Created by ABBC on 4/20/21.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var viewModel: HomeViewModel?
    var viewModel2 = [HomeViewModel]()
    var feedsLocalArray = [HomeFeedDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = HomeViewModel()
        viewModel?.delegate = self
        self.viewModel?.setUpView()
        self.setUpView()
        self.getFeeds()
        
    }
    func setUpView() {
        self.tableView.estimatedRowHeight = 200
        self.tableView.rowHeight = UITableView.automaticDimension
        viewModel?.delegate = self
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    static var storyboardInstance:HomeViewController{
        let storyboard = R.StoryboardRef.Main
        if #available(iOS 13.0, *) {
            let vc = storyboard.instantiateViewController(identifier: "HomeViewController") as!HomeViewController
            return vc
        } else {
            let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as!HomeViewController
            return vc
        }
        
    }
    
    
}

extension HomeViewController: CoordinatorProtocol {
    func hideActivityIndicator(message: String) {
        DispatchQueue.main.async {[weak self] in
            self?.activityIndicator.stopAnimating()
        }
    }
    
    func failure(message: String) {
        
    }
    
    func successResponse(data: [HomeFeedDataModel] , pageNo: Int) {
        //        self.viewModel?.feedsLocalArray = data
        
         let viewModel3 =  data.map({return HomeViewModel(dataObj: $0)})
        self.viewModel2.append(contentsOf: viewModel3)
        viewModel2[0].totalPages = pageNo
        self.viewModel?.feedsLocalArray.append(contentsOf: data)
        //        self.viewModel?.feedsModel = data
        DispatchQueue.main.async { [weak self] in
            self?.tableView.delegate = self
            self?.tableView.dataSource = self
            self?.tableView.reloadData()
        }
        
    }
}
extension HomeViewController:UITableViewDelegate,UITableViewDataSource,HomeNavigationProtocol{
    //MARK:- will display
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == ((viewModel2.count)) - 1 { //
            if  viewModel2.count > 0 {
                if  viewModel2[0].pageNo  < viewModel2[0].totalPages  {
                    viewModel2[0].pageNo += 1
                    self.getFeeds()
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                    guard viewModel2.count > 0 else {
                        return 0
                    }
                    return viewModel2.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        cell.indexPath = indexPath
        let obj = viewModel2[indexPath.item]
        cell.homeViewModel = obj
        cell.delegate = self
        print("viewmodel2 obj \(obj)")
            //        cell.viewModel?.delegate = self
        return cell
        
    }
    func goToUserProfile(indexPath: IndexPath) {
        let vc = UserProfileViewController.storyboardInstance
        let obj = self.viewModel2[indexPath.row]
        vc.userId = obj.dataObj.userID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToCommentSection(indexPath: IndexPath) {
        let vc = CommentsViewController.storyboardInstance
        let obj = self.viewModel2[indexPath.row]
        vc.homeDataModel = obj.dataObj
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
}
/*extension HomeViewController: UITableViewDelegate,UITableViewDataSource,HomeNavigationProtocol{
    //MARK:- will display
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == ((viewModel?.feedsLocalArray.count)!) - 1 { //
            if let viewModels = viewModel {
                if  viewModels.pageNo  < viewModels.totalPages  {
                    viewModel?.pageNo += 1
                    self.getFeeds()
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                if let viewModels = viewModel {
        
                    guard viewModels.feedsLocalArray.count > 0 else {
                        return 0
                    }
                    return viewModels.feedsLocalArray.count
                }
                return  viewModel?.feedsLocalArray.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        cell.indexPath = indexPath
        let obj = viewModel?.feedsLocalArray[indexPath.item]
        cell.delegate = self
        cell.setData(feedDataObj: obj)
        //        cell.viewModel?.delegate = self
        return cell
        
    }
    func goToUserProfile(indexPath: IndexPath) {
        let vc = UserProfileViewController.storyboardInstance
        let obj = viewModel?.feedsLocalArray[indexPath.row]
        vc.userId = obj?.userID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToCommentSection(indexPath: IndexPath) {
        let vc = CommentsViewController.storyboardInstance
        let obj = viewModel?.feedsLocalArray[indexPath.row]
        vc.homeDataModel = obj!
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
}*/
extension HomeViewController{
    func getFeedsDataParams() -> [String:Any] {
       var params:[String:Any] = [:]
        params["page"] = "\(self.viewModel?.pageNo ?? -1)"
        params["limit"] = "\(self.viewModel?.limit ?? -1)"
       return params
   }
     func getFeeds() {
        self.activityIndicator.startAnimating()
        ApiManager.sharedApiManager.getHomeFeeds(url: URLManager.urlSharedInstance.getFeed, getFeedsDataParams())
    }
}
