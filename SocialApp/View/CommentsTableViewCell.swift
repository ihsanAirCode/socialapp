//
//  CommentsTableViewCell.swift
//  SocialApp
//
//  Created by ABBC on 4/22/21.
//

import UIKit

class CommentsTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var postLbl: UILabel!
    var indexPath:IndexPath?
    var commenViewModel:CommentViewModel!{
        didSet{
            self.nameLbl.text = commenViewModel.name
            self.emailLbl.text = commenViewModel.email
            self.postLbl.text = commenViewModel.body

        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(feedDataObj:HomeFeedDataModel?)  {
        self.nameLbl.text = feedDataObj?.name
        self.emailLbl.text = feedDataObj?.email
        self.postLbl.text = feedDataObj?.body
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
