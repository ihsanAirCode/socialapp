//
//  HomeTableViewCell.swift
//  SocialApp
//
//  Created by ABBC on 4/20/21.
//

import UIKit
protocol HomeNavigationProtocol {
    func goToUserProfile(indexPath: IndexPath)
    func goToCommentSection(indexPath: IndexPath)
}

class HomeTableViewCell: UITableViewCell {
    @IBOutlet weak var postLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    var indexPath:IndexPath?
    var delegate:HomeNavigationProtocol?
    var homeViewModel:HomeViewModel!{
        didSet{
            postLbl.text = homeViewModel.body
            titleLbl.text = homeViewModel.title
            nameLbl.text = homeViewModel.name

        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(feedDataObj: HomeFeedDataModel?)  {
        titleLbl.text = feedDataObj?.title
        postLbl.text = feedDataObj?.body
    }
    
    @IBAction func didTapProfileBtn(_ sender: UIButton) {
        delegate?.goToUserProfile(indexPath: self.indexPath ?? IndexPath())
    }
    @IBAction func didTapPost(_ sender: UIButton) {
        delegate?.goToCommentSection(indexPath:self.indexPath ?? IndexPath())

    }
}
