
import Foundation
struct UserDetailBaseDataModel : Codable {
	let code : Int?
	let meta : String?
	let data : UserDetailDataModel?

	enum CodingKeys: String, CodingKey {

		case code = "code"
		case meta = "meta"
		case data = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(Int.self, forKey: .code)
		meta = try values.decodeIfPresent(String.self, forKey: .meta)
		data = try values.decodeIfPresent(UserDetailDataModel.self, forKey: .data)
	}
    init() {
      code = -1
        meta = ""
        data = UserDetailDataModel()
    }
}
