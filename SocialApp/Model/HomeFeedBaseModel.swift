//
//  HomeFeedDataModel.swift
//  SocialApp
//
//  Created by ABBC on 4/21/21.
//

import UIKit
import Foundation

struct HomeFeedBaseModel: Codable {
    let code: Int
    let meta: HomeFeedMetaDataModel
    var data: [HomeFeedDataModel]
    enum CodingKeys: String, CodingKey {
        case code
        case meta
        case data
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code) ?? -1
        meta = try values.decodeIfPresent(HomeFeedMetaDataModel.self, forKey: .meta) ?? HomeFeedMetaDataModel()
        data = try values.decodeIfPresent([HomeFeedDataModel].self, forKey: .data) ?? []
     }
    
    init() {
        code = -1
        meta = HomeFeedMetaDataModel()
        data = []
    }
}
// MARK: - Data-
struct HomeFeedDataModel: Codable {
    let id, userID,post_id: Int
    let title, body, createdAt, updatedAt,name,email: String
    enum CodingKeys: String, CodingKey {
        case id
        case post_id
        case userID = "user_id"
        case title, body
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case name = "name"
        case email = "email"
    }
     init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? -1
        post_id = try values.decodeIfPresent(Int.self, forKey: .post_id) ?? -1
        userID = try values.decodeIfPresent(Int.self, forKey: .userID) ?? -1
        title = try values.decodeIfPresent(String.self, forKey: .title) ?? ""
        body = try values.decodeIfPresent(String.self, forKey: .body) ?? ""
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? ""
        updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? ""
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
        email = try values.decodeIfPresent(String.self, forKey: .email) ?? ""


    }
    init() {
        id = -1
        userID = -1
        title = ""
        body = ""
        createdAt = ""
        updatedAt = ""
        name = ""
        email = ""
        post_id = -1
    }
}
struct HomeFeedMetaDataModel: Codable {
    let pagination: HomeFeedPaginationModel
   
    enum CodingKeys: String, CodingKey {
        case pagination
    }
     init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pagination = try values.decodeIfPresent(HomeFeedPaginationModel.self, forKey: .pagination) ?? HomeFeedPaginationModel()
     }
    init() {
        pagination = HomeFeedPaginationModel()
    }
}

// MARK: - Pagination-
struct HomeFeedPaginationModel: Codable {
    let total, pages, page, limit: Int
    enum CodingKeys: String, CodingKey {
        case total
        case pages
        case page
        case limit
    }
     init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        total = try values.decodeIfPresent(Int.self, forKey: .total) ?? -1
        pages = try values.decodeIfPresent(Int.self, forKey: .pages) ?? -1
        page = try values.decodeIfPresent(Int.self, forKey: .page) ?? -1
        limit = try values.decodeIfPresent(Int.self, forKey: .limit) ?? -1

}
    init() {
        total = -1
        pages = -1
        page = -1
        limit = -1

    }
}
