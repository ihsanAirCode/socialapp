//
//  BaseApiCaller.swift
//  SocialApp
//
//  Created by ABBC on 4/21/21.
//

import UIKit
enum RequestTypeEnum:String{
    case get = "GET"
    case post = "POST"
    
}
class BaseApiCaller: NSObject {
    static let sharedBaseApiManager = BaseApiCaller()
    func createRequest(_ request: inout URLRequest)->URLRequest?{
            request.httpMethod = "GET"
            request.timeoutInterval = 20
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            return request
    
   
    }
    
    //Mark:- Post
    
    fileprivate func createRequest(_ json: [String : Any], _ request: inout URLRequest, _ requestType: RequestTypeEnum?) -> URLRequest? {
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted) as NSData
        request.timeoutInterval = 20
        request.setValue("\(jsonData.length)", forHTTPHeaderField: "Content-Length")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData as Data
        return request
    }
    
    fileprivate func createPostRequest(url:String,requestType:RequestTypeEnum?,_ json: [String : Any], _ request: inout URLRequest) ->URLRequest?{
        if  let url = URL(string: url) {
            var request = URLRequest(url: url)
            switch requestType {
            case .get :
                return json.count>0 ? getQuerryItemsRequest(url: "\(url)", json: json):createRequest(&request)
            case .post:
                return createRequest(json, &request, requestType)
            default:
                break
            }
        }
        else {return nil}
       
      
        return request
    }
    //MARK:-get querry items-
    func getQuerryItemsRequest(url:String,json:[String:Any])->URLRequest? {
            var components = URLComponents(string: url)!
                components.queryItems = json.map { (key, value) in
                    URLQueryItem(name: key, value: value as? String)
                }
                components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
                return URLRequest(url: components.url!)
        }
    //MARK:-post request-
    func post(requestType:RequestTypeEnum?,url:String,parameters:[String: Any],_ completion:@escaping (Data?,Error?,_ statusCode:Int?)->Void){
            var request = URLRequest(url: URL(string: url)!)
        request = createPostRequest(url: url, requestType: requestType, parameters, &request)!
        
            DispatchQueue.main.async {
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, let httpResponse = response as? HTTPURLResponse, error == nil else {
                        completion(nil,error, -1)
                        return
                    }
                    completion(data,nil,httpResponse.statusCode)
                    return 
                }
                task.resume()
            }
        }
}
