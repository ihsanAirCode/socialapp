//
//  UserProfileViewModel.swift
//  SocialApp
//
//  Created by ABBC on 4/22/21.
//

import UIKit
protocol UserProfileApiResponse {
    func userProfileSuccess(model:UserDetailBaseDataModel)
    func hideActivityIndicator(message: String)

}
struct UserProfileViewModel {
    weak var viewController:UserProfileViewController?
    var name = ""
    var email = ""
    var gender = ""
    var userId:Int?
    var delegate:UserProfileApiResponse?
    var userProfileData = UserDetailBaseDataModel()
    
    func setUpView() {
        if let image = UIImage(named: "navBack"){
            let image = image.withTintColor(UIColor.black)
            viewController?.navBackBtn.setImage(image, for: .selected)
        }
        viewController?.navigationController?.setNavigationBarHidden(true, animated: true)
        ApiManager.sharedApiManager.delegate = self

    }
   
    init(model:UserDetailBaseDataModel) {
        self.userProfileData = model
        updateProperties()
    }
    init() {}
    mutating func updateProperties() {
        self.name = self.setName(model:self.userProfileData)
        self.gender = self.setGender(model:self.userProfileData)
        self.email = self.setEmail(model:self.userProfileData)

    }
}
extension UserProfileViewModel:ApiResponseProtocol{
    mutating func success(model: Any?) {
        delegate?.hideActivityIndicator(message: "")
        if let model = model as? UserDetailBaseDataModel{
            self.userProfileData = model
            delegate?.userProfileSuccess(model: model)
        }
    }
    
    func failure(error: String?) {
        delegate?.hideActivityIndicator(message: "")
    }
    
    func renderUserDetailsData(userModel:UserDetailBaseDataModel?) {
        if let data = userModel?.data {
            viewController?.nameValueLbl.text = data.name
            viewController?.emailValueLbl.text = data.email
            viewController?.genderValueLbl.text = data.gender
            switch data.status {
            case "Active":
                viewController?.activeStatusImageView.isHidden = false
                break
            default:
                viewController?.activeStatusImageView.isHidden = true

                break
            }
            
            

        }
    }
    //MARK:-get user details -
    func getUserDetails() {
        self.viewController?.activityIndicator.startAnimating()
        ApiManager.sharedApiManager.getUserDetails(url: "\(URLManager.urlSharedInstance.getUserDetails)\(viewController?.userId ?? -1)", [:])
    }
    
    
    
}

extension UserProfileViewModel{
    private func setName(model:UserDetailBaseDataModel)->String{
        return model.data?.name ?? ""
    }
    private func setGender(model:UserDetailBaseDataModel)->String{
        return model.data?.gender ?? ""
    }
    private func setEmail(model:UserDetailBaseDataModel)->String{
        return model.data?.email ?? ""
    }
    func setUserId(userId:String)->String{
        return userId
    }
}
