//
//  HomeViewModel.swift
//  SocialApp
//
//  Created by ABBC on 4/20/21.
//

import UIKit
protocol CoordinatorProtocol{
    func hideActivityIndicator(message: String)
    func successResponse(data: [HomeFeedDataModel] , pageNo: Int)
    func failure(message: String)
}

struct HomeViewModel: ApiResponseProtocol{
    var limit:Int = 20
    var totalPages:Int = 1
    var pageNo: Int = 1
    var feedsLocalArray: [HomeFeedDataModel] = [HomeFeedDataModel]()
    var feedsModel = HomeFeedBaseModel()
    var dataObj = HomeFeedDataModel()
    var post = ""
    var title = ""
    var body = ""
    var name = ""
   
    var delegate: CoordinatorProtocol?
    
    init(feedModel: HomeFeedBaseModel) {
        self.feedsModel = feedModel
    }
    init(dataObj: HomeFeedDataModel) {
        self.dataObj = dataObj
        self.name = setName()
        self.title = setTitle()
        self.body = setBody()
    }
    init() {
        
    }
    func setUpView() {
        ApiManager.sharedApiManager.delegate = self
        
    }
    
}

//MARK:- api results
extension HomeViewModel{
    mutating func success(model: Any?) {
        delegate?.hideActivityIndicator(message: "Check")
        if let model = model as? HomeFeedBaseModel{
            feedsModel.data = model.data.sorted(by: {$0.updatedAt > $1.updatedAt})
            feedsModel = model
            appendDataWithLocalArray(feedsModel: feedsModel)
        }
    }
    
    func failure(error: String?) {
        delegate?.hideActivityIndicator(message: "")
    }
    
    mutating  func appendDataWithLocalArray(feedsModel:HomeFeedBaseModel?) {
        if let model = feedsModel{
            self.totalPages = model.meta.pagination.pages
            if model.data.count>0 {
                print(self.feedsLocalArray.count)
                delegate?.successResponse(data: model.data , pageNo: self.totalPages)
            }
        }
    }
     
}
extension HomeViewModel{
    private func setName()->String{return self.dataObj.name}
    private func setBody()->String{return self.dataObj.body}
    private func setTitle()->String{return self.dataObj.title}
}
